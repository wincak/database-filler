#include "generator.h"

Generator::Generator(QObject *parent, int min, int max, int periodMs)
    : QObject(parent)
    , m_min(min)
    , m_max(max)
{
    Q_ASSERT(min < max);
    Q_ASSERT(periodMs > 0);

    QTimer* updateTimer = new QTimer(this);
    updateTimer->setInterval(periodMs);

    connect(updateTimer, &QTimer::timeout, this, &Generator::updateValue);

    updateTimer->start();
}

void Generator::updateValue()
{
    m_currentValue++;

    if(m_currentValue > m_max) {
        m_currentValue = m_min;
    }

    emit newValue(m_currentValue);
}

#include "databaseadder.h"

#include <QSqlQuery>
#include <QVariant>

DatabaseAdder::DatabaseAdder(QObject *parent, const QString &connection)
    : QObject(parent)
{
    m_db = QSqlDatabase::database(connection, false);
    if(!m_db.isValid()) {
        qCritical("Database invalid");
    }
}

void DatabaseAdder::addNewValue(int value)
{
    if(!m_db.open()) {
        qCritical("Failed to open database");
        return;
    }

    qDebug("Adding value: %i", value);

    QSqlQuery q(m_db);

    q.prepare("INSERT INTO DataTBL (Value) "
              "VALUES (:value)");
    q.bindValue(":value", QVariant::fromValue(value));
    q.exec();

    m_db.close();
}

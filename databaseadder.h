#ifndef DATABASEADDER_H
#define DATABASEADDER_H

#include <QObject>

#include <QSqlDatabase>

class DatabaseAdder : public QObject
{
    Q_OBJECT

public:
    explicit DatabaseAdder(QObject *parent, const QString& connection);

public slots:
    void addNewValue(int);

private:
    QSqlDatabase m_db;
};

#endif // DATABASEADDER_H

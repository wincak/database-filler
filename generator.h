#ifndef GENERATOR_H
#define GENERATOR_H

#include <QObject>

#include <QTimer>

class Generator : public QObject
{
    Q_OBJECT

public:
    explicit Generator(QObject *parent, int min, int max, int periodMs);

signals:
    void newValue(int);

private slots:
    void updateValue();

private:
    const int m_min;
    const int m_max;

    int m_currentValue;
};

#endif // GENERATOR_H

#include <QCoreApplication>

#include <QSqlDatabase>

#include "databaseadder.h"
#include "generator.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Generator generator(&a, 0, 100, 1000);

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("GeneratorDB");
    db.setUserName("generator");
    db.setPassword("password");

    DatabaseAdder adder(&a, db.connectionName());

    QObject::connect(&generator, &Generator::newValue, &adder, &DatabaseAdder::addNewValue);

    return a.exec();
}
